jQuery(document).ready(function($) {
    $('.my-slider').unslider({ infinite: true, arrows: true, nav: false });

    var initializeProgress = function() {

        var count = new Number(0);

        $('.rating-progress-bar').each(function() {
            var value = new Number($(this).attr('valuenow'));
            count += value;
        });

        $('.rating-progress-bar').each(function() {
            var value = new Number($(this).attr('valuenow'));
            value = (value / count) * 100;
            $(this).css('width', value+'%');
        });

    }
    initializeProgress();

    var initializeModal = function() {
        
        var modal = $('#myModal');
        var modalImageContainer = $('#modalImageContainer');

        $('.slide').on('click', function() {
            var bg = $(this).css('background-image');
            bg = bg.replace('url(','').replace(')','').replace(/\"/g, "");
            modalImageContainer.attr('src', bg);
            modal.show();
        });

        var closeModal = function(e) {
            if (!modalImageContainer.is(e.target) // if the target of the click isn't the container...
                && modalImageContainer.has(e.target).length === 0) // ... nor a descendant of the container
            {
                modal.hide();
            }
        }
        $(document).mouseup(function (e) {
            closeModal(e);
        });
        $(document).on('touchend', function (e) {
            closeModal(e);
        });

        $(document).keyup(function(e) {
            if (e.keyCode == 27) {
                modal.hide();
            }
        });
    };
    initializeModal();
});

